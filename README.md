# Introduction 

The following information has been copied from [APMonitor's](http://apmonitor.com/pdc/index.php/Main/DynamicModeling) Web page, to be used as an intoductory lecture to Git and Python, applied to dynamic modeling[^1].

# Dynamic Model Introduction

Dynamic models are essential for understanding the system dynamics in open-loop (manual mode) or for closed-loop (automatic) control. These models are either derived from data (empirical) or from more fundamental relationships (first principles, physics-based) that rely on knowledge of the process. A combination of the two approaches is often used in practice where the form of the equations are developed from fundamental balance equations and unknown or uncertain parameters are adjusted to fit process data.

In engineering, there are 4 common balance equations from conservation principles including mass, momentum, energy, and species (see Balance Equations). An alternative to physics-based models is to use input-output data to develop empirical dynamic models such as first-order or second-order systems.

[Video 12 Steps for Dynamic Modeling](https://youtu.be/yKEThlGdn0I)

# Steps in Dynamic Modeling

The following are general guidelines for developing a dynamic model. The process is iterative as simulation results help inform modeling assumptions or correct errors in the dynamic balance equations:

1. Identify objective for the simulation
2. Draw a schematic diagram, labeling process variables
3. List all assumptions
4. Determine spatial dependence
	- yes = Partial Differential Equation (PDE)
	- no = Ordinary Differential Equation (ODE)
5. Write dynamic balances (mass, species, energy)
6. Other relations (thermo, reactions, geometry, etc.)
7. Degrees of freedom, does number of equations = number of unknowns?
8. Classify inputs as
	- Fixed values
	- Disturbances
	- Manipulated variables
9. Classify outputs as
	- States
	- Controlled variables
10. Simplify balance equations based on assumptions
11. Simulate steady state conditions (if possible)
12. Simulate the output with an input step

---

# A Beginning Example: 
## Filling a Water Tank

Consider a cylindrical tank with no outlet flow and an adjustable inlet flow. The inlet flow rate is not measured but there is a level measurement that shows how much fluid has been added to the tank. The objective of this exercise is to develop a model that can maintain a certain water level by automatically adjusting the inlet flow rate.

![Diagram of a tank with an inlet and no outlet. The symbol LT is an abbreviation for Level Transmitter.](http://apmonitor.com/pdc/uploads/Main/tank_model_no_outlet.png)

A first step is to develop a dynamic model of how the inlet flow rate affects the level in the tank. A starting point for this model is a [balance equation](http://apmonitor.com/pdc/index.php/Main/PhysicsBasedModels).

The accumulation term is a differential variable such as dm/dt for mass. In this case, the accumulation of mass is equal to only an inlet flow and no outlet, generation, or consumption terms.

![](https://latex.codecogs.com/gif.latex?\frac{dm}{dt}=\dot{m}_{in}-\dot{m}_{out})

## Assumptions
The next objective is to simplify the expression and transform it into a relationship between height _h_ and the valve opening _u_ **(0-100%)**. For liquid water, density is nearly constant even over wide temperature ranges and the mass is equal to the density multiplied by the volume _V_. Assuming a constant cross-sectional area gives _V = h A_ and a linear correlation between valve opening and inlet flow gives the following relationship.

![](https://latex.codecogs.com/gif.latex?\rho&space;A&space;\frac{dh}{dt}=c&space;u)

with 

![](https://latex.codecogs.com/gif.latex?\dot{m}_{in}=cu)

where _c_ is a constant that relates valve opening to inlet flow.

## Problem 

Simulate the height of the tank by integrating the mass balance equation for a period of 10 seconds. The valve opens to 100% at _t=2_ and shuts at _t=7_. Use a value of 1000 kg/m3 for density and 1.0 m2 for the cross-sectional area of the tank. For the valve, assume a valve coefficient of c=50.0 (kg/s / %open).

---

# Python
## Introduction 

Python is an interpreted, **high-level, general-purpose programming language**. Created by *Guido van Rossum and first released in 1991*, Python has a design philosophy that emphasizes code readability, notably using significant whitespace. It provides constructs that enable clear programming on both small and large scales. Van Rossum led the language community until stepping down as leader in July 2018 [^2].

## Python's distributions

There are several python distributions, as well as several ways to install python. Unix system, MacOs and any Linux-Kernel have python already instaled upon the system. However Windows will propably needs an instalation stage. Woever, it is good to know which version you have installed  

---

# Solving differential equations using ODEINT
## Introduction

The _odeint_ function is part of the **scipy** library (complete code on [github](https://github.com/scipy/scipy/blob/v0.17.1/scipy/integrate/odepack.py#L25-L230)). The function allows to integrate a system of ordinary differential equations to solve it. The solving method is done by using **lsoda** from the **FORTRAN library odepack**. There is a great detailed explanation obout the use of _odeint_ in [APMonitor Channel](https://www.youtube.com/watch?v=VV3BnroVjZo). However, a basic introduction will be explained further. But first, let to introduce the differential equations and its applications.



## Simple differential equations and applications

In [^3] a the authors says that "_a differential equation that involves only one independent variable is called an ordinary differential equation (ODE). Those involving two or more independent variables are called partial differential equations (PDEs)._"



## Odeint function
The odeint is function implemented on the **scipy** library (complete code on [github](https://github.com/scipy/scipy/blob/v0.17.1/scipy/integrate/odepack.py#L25-L230)). The function allows to integrate a system of ordinary differential equations. The solve is doneby using **lsoda** from the **FORTRAN library odepack**. 

Solves the initial value problem for stiff or non-stiff systems of first order ode-s:
        
```
dy/dt = func(y, t0, ...)
```

where _y_ can be a vector.

##  Parameters
    func : callable(y, t0, ...)
        Computes the derivative of y at t0.
    y0 : array
        Initial condition on y (can be a vector).
    t : array
        A sequence of time points for which to solve for y.  The initial
        value point should be the first element of this sequence.
    args : tuple, optional
        Extra arguments to pass to function.
    Dfun : callable(y, t0, ...)
        Gradient (Jacobian) of `func`.
    col_deriv : bool, optional
        True if `Dfun` defines derivatives down columns (faster),
        otherwise `Dfun` should define derivatives across rows.
    full_output : bool, optional
        True if to return a dictionary of optional outputs as the second output
    printmessg : bool, optional
        Whether to print the convergence message





# References
[^1]:http://apmonitor.com/pdc/index.php/Main/DynamicModeling "APMonitor"
[^2]:https://en.wikipedia.org/wiki/Python_(programming_language)	"Python (programming language)"
