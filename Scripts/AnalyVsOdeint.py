#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 13:23:47 2019

@author: Marx
"""
#importing libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#time vector
t=np.linspace(0,10)

# Analytical solution
yExact=5*np.exp(-0.3*t)

#model
def model(y,t):
    dydt=-0.3*y
    return dydt

y0=5
#odeint solution
yOde=odeint(model,y0,t)

#Plot

plt.plot(t,yExact,'b--',linewidth=3,label='Exact')
plt.plot(t,yOde,'r-',linewidth=2,label='odeint')
plt.legend()
plt.xlabel('time(s)')
plt.ylabel('y(t)')
plt.show()