#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 14:09:54 2019

@author: Marx
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#time vector
t=np.linspace(0,10)

# Analytical solution
yExact=1.0-1.0*np.exp(-1.0*t)

#model
def model(y,t):
    dydt=-1.0*y+1
    return dydt

y0=0
#odeint solution
yOde=odeint(model,y0,t)

#Plot

plt.plot(t,yExact,'b--',linewidth=3,label='Exact')
plt.plot(t,yOde,'r-',linewidth=2,label='odeint')
plt.legend()
plt.xlabel('time(s)')
plt.ylabel('y(t)')
plt.show()