#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 14:33:16 2019

@author: Marx
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# vectors definition
t=np.linspace(0,50,51)
u=np.zeros(51)
u[10:51]=2
out=np.zeros(51)

#model
def model(y,t,u):
    dydt=1/5*(u-y)
    return dydt

y0=1.
#odeint solution
for i in range(51):
    ux=u[i]
    yOde=odeint(model,y0,[0,1],args=(ux,))
    y0=yOde[-1]
    out[i]=yOde[-1]
    
    

#Plot
plt.plot(t,out,'r-',linewidth=2,label='odeint')
plt.legend()
plt.xlabel('time(s)')
plt.ylabel('y(t)')
plt.show()