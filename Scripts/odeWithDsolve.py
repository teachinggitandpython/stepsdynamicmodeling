#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 20:16:33 2019

@author: Marx
Python program to solve a simple ODE 
"""
from sympy import dsolve, Eq, symbols, Function
t=symbols('t')
x=symbols('x', cls=Function)
deqn1=Eq(x(t).diff(t),1-x(t))
sol1=dsolve(deqn1,x(t))
print(sol1)